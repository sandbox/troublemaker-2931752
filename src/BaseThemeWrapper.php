<?php

namespace ThemeWrapper;

class BaseThemeWrapper {

  protected $var_names = array();

  protected $theme_hook = '';

  protected $render_element = '';

  /**
   * Render the thing.
   *
   * @return string
   *    The HTML.
   */
  public function render() {
    $build = $this->getRenderArray();
    return drupal_render($build);
  }

  /**
   * Get the render array.
   *
   * @return array
   *    The render array.
   */
  public function getRenderArray() {
    $render = array();
    $render['#theme'] = $this->theme_hook;
    if (empty($this->render_element)) {
      foreach ($this->var_names as $var_name) {
        if (isset($this->{$var_name})) {
          $render['#' . $var_name] = $this->{$var_name};
        }
      }
    }
    else {
      $vars = $this->{$this->render_element};
      foreach ($vars as $var_name => $var_value) {
        $render['#' . $var_name] = $var_value;
      }
    }

    return $render;
  }

}
