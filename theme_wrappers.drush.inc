<?php

/**
 * Implements hook_drush_command().
 */
function theme_wrappers_drush_command() {

  $commands['theme-wrapper-generate'] = array(
    'description' => 'Theme wrappers generate.',
    'aliases' => array('th-gen'),
  );

  return $commands;
}

/**
 * Drush command logic.
 * drush_[COMMAND_NAME]().
 */
function drush_theme_wrappers_theme_wrapper_generate() {

  theme_wrappers_clean_src();

  $loader = new Twig_Loader_Filesystem(drupal_get_path('module', 'theme_wrappers') . '/templates');
  $twig = new Twig_Environment($loader);

  $template = $twig->load('theme_wrapper.php.twig');

  $theme_registry = theme_get_registry();

  foreach ($theme_registry as $theme_name => $item) {
    if ($item['type'] == 'module') {

      $stop = 1;

      // Construct class name.
      $theme_name_parts = explode('_', $theme_name);
      $class_name = '';
      foreach ($theme_name_parts as $theme_name_part) {
        $class_name .= ucfirst($theme_name_part);
      }
      $class_name .= 'ThemeWrapper';

      // Get theme variables
      $vars = array();
      $var_names = '';
      $render_element = NULL;
      if (isset($item['variables'])) {
        foreach ($item['variables'] as $variable_name => $variable_type) {
          $vars[$variable_name] = var_export($variable_type, TRUE);
        }
        $var_names = var_export(array_keys($item['variables']), TRUE);
      }
      elseif (isset($item['render element'])) {
        $var_names = var_export(array($item['render element']), TRUE);
        $vars[$item['render element']] = var_export(NULL, TRUE);
        $render_element = $item['render element'];
      }

      $class = $template->render(array('class_name' => $class_name, 'theme_type' => $theme_name, 'vars' => $vars, 'var_names' => $var_names, 'render_element' => $render_element));
      file_put_contents(theme_wrappers_get_src_path() . '/' .  $class_name . '.php', $class);
    }
  }


}
